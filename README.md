<div align="center">

 <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Istio-bluelogo-nobackground-unframed.svg/512px-Istio-bluelogo-nobackground-unframed.svg.png" alt="Logo do Istio" width="15%" />

 
 # GCP with Istio
 
</div>

Este projeto tem a responsabilidade de armazenar todos os códigos e documentação relacionados aos experimentos realizados com a Google Cloud e Istio. O principal objetivo deste projeto é estabelecer um ambiente de service mesh.

## Requisitos

Antes de começar, certifique-se de atender aos seguintes requisitos:

- Uma conta ativa na Google Cloud Platform.
- Certifique-se de ter as seguintes ferramentas instaladas:
    - Google Cloud CLI
    - Kubectl
    - Helm
    - Curl
- Para ambientes Windows, é recomendado ter as seguintes ferramentas:
    - WSL (Windows Subsystem for Linux)
    - Uma distribuição Linux, preferencialmente Ubuntu.

## Etapas

A seguir, apresentamos as etapas principais para iniciar e realizar experimentos com as configurações do Istio na GCP:

### Etapa 1 - Criação e Configuração do Cluster

1. Defina o projeto e a região da Google Cloud e verifique se a API do Google Kubernetes Engine está ativada. Exporte as variáveis de ambiente necessárias:

```shell
export PROJECT_ID=`gcloud config get-value project`
export REGION="us-central1-b"
export CLUSTER_NAME="scnt-poc-observable-cluster"
```

2. Habilite as APIs necessárias:

```shell
gcloud services enable container.googleapis.com \
monitoring.googleapis.com \
cloudtrace.googleapis.com \
--project=${PROJECT_ID}
```

3. Crie um cluster GKE e obtenha as credenciais para ele:

```shell
gcloud container clusters create ${CLUSTER_NAME} \
--project=${PROJECT_ID} --zone=${REGION} \
--machine-type=e2-standard-2 \
--num-nodes=2
```

4. Conecte-se ao cluster:

```shell
gcloud container clusters get-credentials ${CLUSTER_NAME} \
--zone=${REGION} \
--project=${PROJECT_ID}
```

5. Conceda permissões de administrador do cluster ao usuário atual:

```shell
kubectl create clusterrolebinding cluster-admin-binding \
--clusterrole=cluster-admin \
--user=$(gcloud config get-value core/account)
```

## Etapa 2 - Instalação do Istio

Para continuar, é necessário executar os próximos comandos em um ambiente de distribuição Linux. Use o seguinte comando para baixar os arquivos de instalação para o seu sistema operacional (Linux ou macOS):

1. Instalação do istioctl:

```shell
curl -L https://istio.io/downloadIstio | sh -
```

2. Acesse o diretório da ferramenta que você acabou de baixar:

```shell
cd istio-1.19.3
```

3. Adicione o comando ao seu path:

```shell
export PATH="$PATH:~/istio-1.19.3/bin"
```

4. Verifique se o comando foi instalado com sucesso:

```shell
istioctl version
```

5. Instale o Istio no cluster:

```shell
istioctl install --set profile=demo -y
```

6. Habilite a injeção automática do Istio em todos os pods no namespace **default**:

```shell
kubectl label namespace default istio-injection=enabled
```

### Etapa 3 - Instalação de complementos para o Istio

Instale componentes adicionais que fornecerão informações detalhadas sobre o funcionamento de suas aplicações:

```shell
kubectl apply -f ~/istio-1.19.3/samples/addons
kubectl rollout status deployment/kiali -n istio-system
```

### Etapa 4 - Instalação dos microsserviços

Aqui, você instalará os microsserviços necessários para obter visibilidade e observabilidade. A Boutique Online é um aplicativo de comércio eletrônico de microsserviços baseado na web.

1. Clone o repositório:

```shell
git clone https://github.com/GoogleCloudPlatform/microservices-demo
cd microservices-demo/
```

2. Implante a Boutique Online no cluster:

```shell
kubectl apply -f ./release/kubernetes-manifests.yaml
```

3. Aguarde a conclusão da implantação dos pods:

```shell
kubectl get pods
```

Depois de alguns minutos, os pods devem estar em estado **Running**.

4. Implante as configurações para o Istio:

```shell
kubectl apply -f ./istio-manifests
```

5. Acesse o frontend da Web em um navegador usando o IP externo do frontend:

```shell
kubectl get service frontend-external | awk '{print $4}'
```

Copie o endereço de IP gerado e cole-o em seu navegador.

### Etapa 5 - Acessando o Kiali

Agora que os aplicativos estão implantados, acesse o Kiali para obter informações detalhadas sobre sua malha de serviços:

```shell
istioctl dashboard kiali
```

Exemplo:

![kiale](./assets/img/kiale.PNG)

### Etapa 6 - Limpar Recursos de Demonstração

Quando terminar seus experimentos e quiser limpar os recursos implantados, execute o seguinte comando:

```shell
gcloud container clusters delete ${CLUSTER_NAME} \
  --project=${PROJECT_ID} --region=${REGION}
```

Isso removerá todos os recursos criados nas etapas anteriores. A exclusão do cluster pode levar alguns minutos.

## Conclusão

Esta documentação tem o objetivo de orientar os usuários na implantação inicial do Istio na Google Cloud. Os passos apresentados aqui são básicos e introdutórios. Se você estiver interessado em explorar ainda mais, consulte os links a seguir para documentação mais avançada sobre o Istio.

## Referências

- [Documentação do Istio](https://istio.io/latest/docs/setup/getting-started/)
- [Descomplicando o Istio - Aula ao Vivo](https://youtu.be/Qk7FFBby43U)
- [Entendendo Istio e Service Mesh](https://youtu.be/Txv9CQoUEL8)
- [Microsserviços com Service Mesh: Kubernetes e Istio](https://youtu.be/O0m02aLBHrQ)
- [What is ServiceMesh and specially what is Istio?](https://isitobservable.io/observability/service-mesh/what-is-servicemesh-and-specially-what-is-istio)
- [GCP microservices-demo](https://github.com/GoogleCloudPlatform/microservices-demo)
- [Terraform with Istio By ReinanHS](https://github.com/ReinanHS/terraform-minikube-istio/tree/main)